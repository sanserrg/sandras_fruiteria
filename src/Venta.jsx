import React from 'react';
import { Card, Button, CardTitle, CardText } from 'reactstrap';
import styled from 'styled-components';

const Venta = (props) => {

    const Marco = styled.div`
        width: 300px;
        border-radius: 25%;
        padding: 1.5px;
    `;

    const estilo = { 
        position: "absolute",
        marginLeft: "720px",
        top: "10px"
    };
    
    let lista = props.lista;
    let productos = [...lista].filter(el => el.unitats > 0).map((el, i) => (
        <Marco>
            <Card key={i} body inverse style={{ backgroundColor: '#333', borderColor: '#333' }}>
                <CardTitle>Producto: {el.nom}</CardTitle>
                <CardText>Precio/u: {el.preu}€ x {el.unitats} unid. = {el.preu * el.unitats}€</CardText>
                <CardText>Unidades: {el.unitats}</CardText>
                <Button onClick={() => props.eliminar(el)}>Eliminar</Button>
            </Card>
        </Marco>
    ));

    return (
        <>
            <div style={estilo}>{productos}</div>

        </>
    );
};

export default Venta;