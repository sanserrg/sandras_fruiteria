import { React } from 'react';
import { Card, CardTitle } from 'reactstrap';
import styled from 'styled-components';

const Ticket = (props) => {

    const estilo = {
        marginLeft: "720px",
        top: "60px",
        position: 'absolute'
    };

    const Marco = styled.div`
        width: 300px;
        border-radius: 25%;
        padding: 1.5px;
        display: inline-block;
    `;

    let total = props.total;

    let PrecioTotal = (
        <Marco>
            <Card body inverse style={{ backgroundColor: '#333', borderColor: '#333' }}>
                <CardTitle>Total: {total}€</CardTitle>
            </Card>
        </Marco>
    );

    return (
        <>
            <div style={estilo}>
                {PrecioTotal}
            </div>
        </>
    );
};

export default Ticket;