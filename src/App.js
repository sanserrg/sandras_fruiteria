import { React, useState } from 'react';
import Productos from './Productos';
import Ticket from './Ticket';
import Venta from './Venta';
import styled from 'styled-components';

export default () => {

  
  let productes = [
    {
      "id": 1,
      "nom": "Plátano",
      "preu": 0.5,
      "unitats": 0,
    },
    {
      "id": 2,
      "nom": "Manzana",
      "preu": 1,
      "unitats": 0,
    },
    {
      "id": 3,
      "nom": "Melocotón",
      "preu": 3.5,
      "unitats": 0,
    },
    {
      "id": 4,
      "nom": "Pera",
      "preu": 2,
      "unitats": 0,
    },
    {
      "id": 5,
      "nom": "Piña",
      "preu": 3.5,
      "unitats": 0,
    },
    {
      "id": 6,
      "nom": "Sandía",
      "preu": 3,
      "unitats": 0,
    },
  ];

  const [total, setTotal] = useState(0);
  const [lista, setLista] = useState(productes)
  
  const añadir = (x) => {
    setTotal(total + x.preu)
    const nuevaLista = lista.map(
      el => {
        if (el.id === x.id) {
          el.unitats = el.unitats + 1
        } return el;
      }
    )
    setLista(nuevaLista);
  }

  const eliminar = (x) => {
    setTotal(total - x.preu * x.unitats)
    const nuevaLista = lista.map(
      el => {
        if (el.id === x.id) {
          el.unitats = 0
        } return el;
      }
    )
    setLista(nuevaLista);
  }


  const Titulo = styled.h2`
    left: 40%;
    position: absolute;
  `;

  return (
    <>
      <Titulo>Frutería</Titulo>
      <Productos productos={productes} añadir={añadir} />
      <Venta lista={lista} eliminar={eliminar} />
      <Ticket total={total} />
    </>
  )
}