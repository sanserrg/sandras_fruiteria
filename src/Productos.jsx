import React from 'react';
import { Card, Button, CardTitle, CardText } from 'reactstrap';
import styled from 'styled-components';

const Productos = (props) => {

    const Marco = styled.div`
        width: 300px;
        border-radius: 25%;
        padding: 1.5px;
    `;

    const estilo = { 
        display: "inline-block", 
        position: "absolute", 
        marginLeft: "160px", 
        top: "60px" 
    };
    
    let productes = props.productos;
    let productos = [...productes].map((el, i) => (
        <Marco>
            <Card key={i} body inverse style={{ backgroundColor: '#333', borderColor: '#333' }}>
                <CardTitle>Producto: {el.nom}</CardTitle>
                <CardText>Precio: {el.preu}€</CardText>
                <Button onClick={() => props.añadir(el)}>Añadir</Button>
            </Card>
        </Marco>
    ));

    return (
        <>
            <div style={estilo}>{productos}</div>

        </>
    );
};

export default Productos;